class Card:
    title: str = None
    content: str = None

    def __init__(self, title: str = None, content: str = None):
        self.title = title
        self.content = content
