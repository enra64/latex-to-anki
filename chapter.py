import json
import logging
import re
from typing import Dict, List

from card import Card


class Chapter:
    CONTENT_MARKER = "__CONTENT__"

    # \includegraphics[width=0.7\linewidth]{communication-dataflow-spectrum}
    INCLUDEGRAPHICS = r"\\includegraphics.*?\{(.*?)\}"
    FIGURE = r"\\begin\{figure\}(.*?)\\end\{figure\}"  # single line flag!
    CAPTION = r"\\caption\[(.*?)\]\{(.*)\}"
    ITEMIZE = r"\\begin\{itemize\}(.*?)\\end\{itemize\}"  # single line flag!
    ENUMERATE = r"\\begin\{enumerate\}(.*?)\\end\{enumerate\}"
    ITALICS = r"\\textit\{(.*?)\}"
    BOLD = r"\\textbf\{(.*?)\}"
    MONOSPACE = r"\\texttt\{(.*?)\}"
    ITEM = r"\t*\\item(.*?)$"
    ITEM_PARAMETRIZED = r"\t*\\item ?\[(.*?)\] ?(.*?)$"
    RIGHT_ARROW = r"\$\\rightarrow\$"
    INLINE_MATH = r"\$(.*?)\$"
    SUBSCRIPT_SINGLE = r"$[^\\]_(.)$"
    SUBSCRIPT_MULTIPLE = r"[^\\]_\{(.+)\}"
    SUPERSCRIPT_SINGLE = r"$[^\\]\^(.)$"
    SUPERSCRIPT_MULTIPLE = r"[^\\]\^\{(.+)\}"

    latex_replacements = None
    included_graphics: List[str] = None
    sections: Dict = None
    strip_fail_count = 0

    def __init__(self):
        self.included_graphics = []
        self.sections = {}
        with open("latex-unicode-replacements.json", "r") as file:
            self.latex_replacements = json.load(file)
            self.latex_replacements["\\ne"] = "≠"
            self.latex_replacements["\\%"] = "%"
            self.latex_replacements["\\#"] = "#"

    def get_section(self, section_name: str):
        if section_name not in self.sections:
            self.sections[section_name] = {}

        return self.sections[section_name]

    def get_subsection(self, section_name: str, subsection_name: str):
        section = self.get_section(section_name)

        if subsection_name not in section:
            section[subsection_name] = {}

        return section[subsection_name]

    def get_subsubsection(self, section_name: str, subsection_name: str, subsubsection_name: str):
        subsection = self.get_subsection(section_name, subsection_name)

        if subsubsection_name not in subsection:
            subsection[subsubsection_name] = {}

        return subsection[subsubsection_name]

    def get_paragraph(self, section_name: str, subsection_name: str, subsubsection_name: str, paragraph_name: str):
        subsubsection = self.get_subsubsection(section_name, subsection_name, subsubsection_name)

        if paragraph_name not in subsubsection:
            subsubsection[paragraph_name] = []

        return subsubsection[paragraph_name]

    def _replace_characters(self, text: str):
        replacements = {}
        for match in re.finditer(self.INLINE_MATH, text):
            latex_command = match.group(1)
            if latex_command.strip() in self.latex_replacements:
                replacements["${}$".format(latex_command)] = self.latex_replacements[latex_command.strip()]

        for latex, replacement in replacements.items():
            text = text.replace(latex, replacement)

        return text

    def strip_latex(self, content: str) -> str:
        original_copy = content

        while re.search(self.ITEMIZE, content, flags=re.S):
            content = re.sub(self.ITEMIZE, r"<ul>\1</ul>", content, flags=re.S)
        while re.search(self.ENUMERATE, content, flags=re.S):
            content = re.sub(self.ENUMERATE, r"<ol>\1</ol>", content, flags=re.S)

        while re.search(self.SUBSCRIPT_SINGLE, content):
            content = re.sub(self.SUBSCRIPT_SINGLE, r"<sub>\1</sub>", content)
        while re.search(self.SUBSCRIPT_MULTIPLE, content):
            content = re.sub(self.SUBSCRIPT_MULTIPLE, r"<sub>\1</sub>", content)
        while re.search(self.SUPERSCRIPT_SINGLE, content):
            content = re.sub(self.SUPERSCRIPT_SINGLE, r"<sup>\1</sup>", content)
        while re.search(self.SUPERSCRIPT_MULTIPLE, content):
            content = re.sub(self.SUPERSCRIPT_MULTIPLE, r"<sup>\1</sup>", content)

        while re.search(self.ITALICS, content) or re.search(self.BOLD, content) or re.search(self.MONOSPACE, content):
            content = re.sub(self.ITALICS, r"<i>\1</i>", content)
            content = re.sub(self.BOLD, r"<b>\1</b>", content)
            content = re.sub(self.MONOSPACE, r"<code>\1</code>", content)
        content = re.sub(self.ITEM_PARAMETRIZED, r"<li><b>\1</b>: \2</li>", content, flags=re.M)
        content = re.sub(self.ITEM, r"<li>\1</li>", content, flags=re.M)

        content = self._replace_characters(content)

        if self._contains_latex(content):
            return original_copy

        return content

    @staticmethod
    def _contains_latex(text: str):
        return "$" in text or "{" in text or "[" in text

    def strip_latex_title(self, title: str) -> str:
        title = re.sub(self.ITALICS, r"<i>\1</i>", title)
        title = re.sub(self.BOLD, r"<b>\1</b>", title)
        title = re.sub(self.MONOSPACE, r"<code>\1</code>", title)
        return title

    def _figure_format(self, title, content) -> List[Card]:
        figure_card = Card()
        figure_card.title = title

        long_caption = ""
        if re.search(self.CAPTION, content):
            caption = re.findall(self.CAPTION, content)[0]
            short_caption = caption[0]
            long_caption = caption[1]
            figure_card.title += " > " + short_caption

        figure_card.content = long_caption + "\n\n\n"

        image_file = re.findall(self.INCLUDEGRAPHICS, content)[0]

        self.included_graphics.append(image_file)
        figure_card.content += '<img src="{}">'.format(image_file)

        content_without_figure = re.sub(self.FIGURE, r"", content, flags=re.S)
        rest_card = Card(title, content_without_figure)

        return [figure_card, rest_card]

    def format_card(self, chapter, section, subsection, subsubsection, paragraph, content) -> List[Card]:
        title_path = [chapter, section, subsection, subsubsection, paragraph]
        title = self.strip_latex_title(" > ".join([path_item for path_item in title_path if path_item is not None]))
        content = "".join(content)
        stripped_content = self.strip_latex(content)

        if self._contains_latex(stripped_content):
            logging.debug("did not completely strip latex from {}".format(title))
            self.strip_fail_count += 1
            logging.debug(stripped_content)

        if stripped_content.isspace():
            return []

        if re.search(self.INCLUDEGRAPHICS, stripped_content):
            return self._figure_format(title, stripped_content)
        else:
            return [Card(title, stripped_content)]

    def to_card_list(self, chapter_name) -> List[Card]:
        card_list = []
        self.strip_fail_count = 0

        for section_name, section in self.sections.items():
            if section_name == self.CONTENT_MARKER:
                card_list.extend(self.format_card(chapter_name, None, None, None, None, section[self.CONTENT_MARKER]))
            else:
                for subsection_name, subsection in section.items():
                    if subsection_name == self.CONTENT_MARKER:
                        card_list.extend(self.format_card(chapter_name, section_name, None, None, None, subsection))
                    else:
                        for subsubsection_name, subsubsection in subsection.items():
                            if subsubsection_name == self.CONTENT_MARKER:
                                card_list.extend(
                                    self.format_card(chapter_name, section_name, subsection_name, None, None,
                                                     subsubsection))
                            else:
                                for paragraph_name, paragraph in subsubsection.items():
                                    if paragraph_name == self.CONTENT_MARKER:
                                        card_list.extend(self.format_card(chapter_name, section_name, subsection_name,
                                                                          subsubsection_name, None,
                                                                          paragraph))
                                    else:
                                        card_list.extend(self.format_card(chapter_name, section_name, subsection_name,
                                                                          subsubsection_name, paragraph_name,
                                                                          paragraph))

        logging.info("Could not strip latex from {} cards".format(self.strip_fail_count))
        return card_list

    def put_content(self, section: str, subsection: str, subsubsection: str, paragraph: str, content: str):
        if section and not subsection and not subsubsection and not paragraph:
            if self.CONTENT_MARKER not in self.get_section(section):
                self.get_section(section)[self.CONTENT_MARKER] = []
            self.get_section(section)[self.CONTENT_MARKER].append(content)
        elif section and subsection and not subsubsection and not paragraph:
            if self.CONTENT_MARKER not in self.get_subsection(section, subsection):
                self.get_subsection(section, subsection)[self.CONTENT_MARKER] = []
            self.get_subsection(section, subsection)[self.CONTENT_MARKER].append(content)
        elif section and subsection and subsubsection and not paragraph:
            if self.CONTENT_MARKER not in self.get_subsubsection(section, subsection, subsubsection):
                self.get_subsubsection(section, subsection, subsubsection)[self.CONTENT_MARKER] = []
            self.get_subsubsection(section, subsection, subsubsection)[self.CONTENT_MARKER].append(content)
        elif section and subsection and subsubsection and paragraph:
            self.get_paragraph(section, subsection, subsubsection, paragraph).append(content)
        else:
            logging.warning(
                "invalid path spec {},{},{},{} for put_content".format(section, subsection, subsubsection, paragraph)
            )
