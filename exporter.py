import os
import re
import zipfile
from shutil import copyfile
from typing import List, Tuple

import anki
from anki import Collection

from card import Card


def _add_default_card_template(mm, m):
    t = mm.newTemplate("Basic Card")
    t['qfmt'] = "{{Front}}"
    t['afmt'] = "{{FrontSide}}\n\n<hr id=answer>\n\n" + "{{Back}}"
    mm.addTemplate(m, t)
    return t


def _add_latex_card_template(mm, m):
    t = mm.newTemplate("Latex Card")
    t['qfmt'] = "{{Front}}"
    t['afmt'] = "{{FrontSide}}\n\n<hr id=answer>\n\n" + "{{Back}}"
    mm.addTemplate(m, t)


def _add_ddm_model(collection: Collection):
    ddm_model_candidate = collection.models.byName("DDM_Model")
    if ddm_model_candidate is None:
        model = collection.models.new("DDM_Model")
        collection.models.addField(model, collection.models.newField("Front"))
        collection.models.addField(model, collection.models.newField("Back"))
        default_card_template = _add_default_card_template(collection.models, model)
        # _add_latex_card_template(mm, m)
        collection.models.add(model)
        collection.models.save(model, [default_card_template])
        return model
    else:
        collection.models.setCurrent(ddm_model_candidate)
        return ddm_model_candidate


def _write_export_file(target: str, database_path: str, media_file_path: str, media_files_target_directory):
    zipf = zipfile.ZipFile(target, 'w', zipfile.ZIP_DEFLATED)
    zipf.write(media_file_path, "media")
    zipf.write(database_path, "collection.anki2")
    for media_file_name in os.listdir(media_files_target_directory):
        zipf.write(os.path.join(media_files_target_directory, media_file_name), os.path.basename(media_file_name))

    zipf.close()


def _write_database(target: str, card_list: List[Card]):
    collection = anki.Collection(target)
    _add_ddm_model(collection)
    deck_id = collection.decks.id("Distributed Data Management")
    deck = collection.decks.get(deck_id)

    for card in card_list:
        note = collection.newNote()
        note['Front'] = card.title

        if "{" in card.content or "$" in card.content:
            if "img" in card.content:
                card_content_without_img = re.sub(r'<img src=".*?">', "", card.content)
                card_images = re.findall(r'<img src=".*?">', card.content)
                note['Back'] = "[latex]" + card_content_without_img + "[/latex]" + "\n".join(card_images)
            else:
                note['Back'] = "[latex]" + card.content + "[/latex]"
        else:
            note['Back'] = card.content

        collection.addNote(note)

    collection.close()


def _write_media_file(media_file_path: str, media_file_names: List[Tuple[str, str]]):
    template = '"{}": "{}"'
    media_files = []

    for media_file_name in media_file_names:
        media_files.append(template.format(media_file_name[0] + "." + media_file_name[1], media_file_name[0]))

    with open(media_file_path, "w") as media_file:
        media_file.write("{" + ",".join(media_files) + "}")

    return media_files


def _mkdir_p(paths: List[str]):
    for path in paths:
        path = os.path.dirname(path)
        if path != "" and not os.path.exists(path):
            os.makedirs(path)


def _copy_figures(media_file_source_directory: str, target_directory: str, figure_list: List[str]):
    extensions = ["jpg", "pdf", "png"]
    figure_names = []
    for figure_name in figure_list:
        for extension in extensions:
            candidate_file_name = "{}.{}".format(figure_name, extension)
            candidate = os.path.join(media_file_source_directory, candidate_file_name)
            if os.path.exists(candidate):
                figure_names.append((figure_name, extension))
                copyfile(candidate, os.path.join(target_directory, candidate_file_name))

    return figure_names


def export(target: str, card_list: List[Card], media_files: List[str], media_file_source_directory: str):
    database_path = "/tmp/latex-to-anki/collection.anki2"
    media_file_path = "/tmp/latex-to-anki/media_description.txt"
    media_files_target_directory = "/tmp/latex-to-anki/media_files/"

    _mkdir_p([database_path, media_file_path, media_files_target_directory, target])

    if os.path.exists(database_path):
        os.remove(database_path)

    cwd = os.getcwd()
    _write_database(database_path, card_list)

    media_file_locations = _copy_figures(media_file_source_directory, media_files_target_directory, media_files)
    _write_media_file(media_file_path, media_file_locations)

    os.chdir(cwd)
    _write_export_file(target, database_path, media_file_path, media_files_target_directory)
