import logging
import re
from functools import reduce
from typing import List, Tuple, Dict

from card import Card
from chapter import Chapter

chapter_regex = r"\\chapter\{(.*?)\}.*?\n"
section_regex = r"\\section\{(.*?)\}.*?\n"
subsection_regex = r"\\subsection\{(.*?)\}.*?\n"
subsubsection_regex = r"\\subsubsection\{(.*?)\}.*?\n"
paragraph_regex = r"\\paragraph\{(.*?)\}.*?\n"


def update_path_spec(path_spec: [str, str, str, str, str], line: str) -> bool:
    if re.fullmatch(chapter_regex, line):
        chapter = re.findall(chapter_regex, line)[0]
        path_spec[0] = chapter
        path_spec[1:5] = [None] * 4
    elif re.fullmatch(section_regex, line):
        path_spec[1] = re.findall(section_regex, line)[0]
        path_spec[2:5] = [None] * 3
    elif re.fullmatch(subsection_regex, line):
        path_spec[2] = re.findall(subsection_regex, line)[0]
        path_spec[3:5] = [None] * 2
    elif re.fullmatch(subsubsection_regex, line):
        path_spec[3] = re.findall(subsubsection_regex, line)[0]
        path_spec[4:5] = [None]
    elif re.fullmatch(paragraph_regex, line):
        path_spec[4] = re.findall(paragraph_regex, line)[0]
    else:
        # logging.debug("{} is not a path spec changer".format(line))
        return False

    # logging.info("changed path spec to {}".format(str(path_spec)))
    return True


def read_chapter(path: str) -> Tuple[List[Card], List[str]]:
    # chapter -> section -> subsection -> subsubsection -> paragraph
    path_spec: [str, str, str, str, str] = [None, None, None, None, None]
    chapters: Dict[str, Chapter] = {}

    with open(path, "r") as file:
        for line in file:
            was_section_mark = update_path_spec(path_spec, line)
            if not was_section_mark:
                if path_spec[0] not in chapters:
                    chapters[path_spec[0]] = Chapter()

                chapters[path_spec[0]].put_content(*path_spec[1:5], line)

    figure_names = []
    cards = []
    for (chapter_name, chapter) in chapters.items():
        cards.extend(chapter.to_card_list(chapter_name))
        figure_names.extend(chapter.included_graphics)

    return cards, figure_names


def read_all(path: str) -> List[Card]:
    input_regex = r"\\input\{(.*?)\}"
    with open(path, "r") as file:
        text = file.read()
        content_file_names = re.findall(input_regex, text)

    card_list = []
    for content_file_name in content_file_names:
        chapter_cards = read_chapter(content_file_name)
        card_list.extend(chapter_cards)

    return card_list
