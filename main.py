import logging
import os
from typing import List, Tuple

import exporter
import importer
from card import Card


def read_multiple(file_list: List[str]) -> Tuple[List[Card], List[str]]:
    card_list = []
    graphics_list = []

    for file in file_list:
        import_data = importer.read_chapter(file)
        logging.info("imported {} with {} cards".format(os.path.basename(file), len(import_data[0])))
        card_list.extend(import_data[0])
        graphics_list.extend(import_data[1])

    return card_list, graphics_list


def read_directory(dir: str) -> Tuple[List[Card], List[str]]:
    file_list = []
    for file in os.listdir(dir):
        if file.endswith(".tex"):
            if not "main.tex" in file:
                file_list.append(os.path.join(dir, file))

    return read_multiple(file_list)


def read_single() -> Tuple[List[Card], List[str]]:
    file = "/home/arne/Documents/git-repos/master/WS1920/DDM/ddm-summary/00_DDM_Introduction.tex"
    return importer.read_chapter(file)


def read_fixed_multiple():
    return read_multiple([
        "/home/arne/Documents/git-repos/master/WS1920/DDM/ddm-summary/06_DDM_Replication.tex"
    ])


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    # [card_list, graphics_list] = read_single()
    [card_list, graphics_list] = read_fixed_multiple()
    # [card_list, graphics_list] = importer.read_chapter("test/res/test_includegraphics_figure.tex")
    # [card_list, graphics_list] = importer.read_chapter("test/res/test_nested_itemize.tex")
    # [card_list, graphics_list] = read_directory("/home/arne/Documents/git-repos/master/WS1920/DDM/ddm-summary/")
    exporter.export("cards_6.apkg", card_list, graphics_list,
                    "/home/arne/Documents/git-repos/master/WS1920/DDM/ddm-summary/")
    logging.info("Exported {} cards".format(len(card_list)))
